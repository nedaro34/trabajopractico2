package librojava;

public class Libro{
    private String isbn;
    private String titulo;
    private String autor;
    private Integer numeroPaginas;

    public Libro(String isbn, String titulo, String autor, Integer numeroPaginas) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.autor = autor;
        this.numeroPaginas = numeroPaginas;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
    public String getTitulo() {
        return titulo;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getAutor() {
        return autor;
    }

    public void setNumeroPaginas(Integer numeroPaginas) {
        this.numeroPaginas = numeroPaginas;
    }

    public Integer getNumeroPaginas() {
        return numeroPaginas;
    }

    public void mostrarInformacion(){
        System.out.println("El libro con ISBN "+isbn+" creado por el autor: "+autor+", tiene "+numeroPaginas+" páginas");
    }
}