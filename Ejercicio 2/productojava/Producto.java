public class Producto{
    private String codigo;
    private String nombre;
    private Integer precioCosto;
    private Integer porcentajeGanancia;
    private static Integer iva = 21;
    private Integer precioVenta;

    public Producto(String codigo, String nombre, Integer precioCosto, Integer porcentajeGanancia) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.precioCosto = precioCosto;
        this.porcentajeGanancia = porcentajeGanancia;
        this.precioVenta = Utilidades.calcularPrecioVenta(iva, precioCosto, porcentajeGanancia);
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
    
    public void setPrecioCosto(Integer precioCosto) {
        this.precioCosto = precioCosto;
    }

    public Integer getPrecioCosto() {
        return precioCosto;
    }
    
    public void setPorcentajeGanancia(Integer porcentajeGanancia) {
        this.porcentajeGanancia = porcentajeGanancia;
    }

    public Integer getPorcentajeGanancia() {
        return porcentajeGanancia;
    }
    
    public static void setIva(Integer iva) {
        Producto.iva = iva;
    }

    public static Integer getIva() {
        return iva;
    }
    
    public void setPrecioVenta(Integer precioVenta) {
        this.precioVenta = precioVenta;
    }

    public Integer getPrecioVenta() {
        return precioVenta;
    }

    public void mostrarInformacion(){
        System.out.println("Producto: "+getNombre());
        System.out.println("Código: "+getCodigo());
        System.out.println("Precio de costo: "+getPrecioCosto().toString());
        System.out.println("Porcentaje de ganancia: "+getPorcentajeGanancia().toString());
        System.out.println("Iva aplicado: "+getIva().toString());
        System.out.println("Precio de venta: "+getPrecioVenta());
    }
}