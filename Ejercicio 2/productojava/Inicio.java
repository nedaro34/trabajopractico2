public class Inicio {
    public static void main(String[] args){
        Producto producto1 = new Producto("d112e2", "cuchara", 10, 10);
        Producto producto2 = new Producto("cwr212", "tenedor", 12, 11);

        producto1.mostrarInformacion();
        producto2.mostrarInformacion();

        if(producto1.getPrecioVenta()>producto2.getPrecioVenta()){
            System.out.println("El producto "+producto1.getNombre()+" tiene un precio mayor que el producto "+producto2.getNombre());
        } else if(producto1.getPrecioVenta()<producto2.getPrecioVenta()){
            System.out.println("El producto "+producto2.getNombre()+" tiene un precio mayor que el producto "+producto1.getNombre());
        } else if(producto1.getPrecioVenta()==producto2.getPrecioVenta()){
            System.out.println("Ambos productos tienen el mismo precio");
        }
    }
}